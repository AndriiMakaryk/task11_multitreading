package threads.pingPongGame;

public class Ping extends Thread {
    private Table table;
    private String textOne="";

    public Ping(Table table) {
        this.table = table;
    }

    public void run() {
        for(int i=0;i<=15;i++){
            textOne=table.getPing();
            System.out.println(i+" "+textOne);
        }
    }
}
