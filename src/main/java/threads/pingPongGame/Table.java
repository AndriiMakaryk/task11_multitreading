package threads.pingPongGame;

public class Table extends Thread {
    private Table table;
    private boolean pigned = false;

    public synchronized String getPong() {
        while (pigned == false) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        pigned = false;
        notifyAll();
        return "pong";
    }

    public synchronized String getPing() {
        while (pigned == true) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        pigned=true;
        notifyAll();
        return "ping";
    }
}
