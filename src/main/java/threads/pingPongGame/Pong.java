package threads.pingPongGame;

public class Pong extends Thread {
    private Table table;
    private String textTwo = "";

    public Pong(Table table) {
        this.table = table;
    }

    public void run() {
        for (int i = 0; i <= 15; i++) {
            textTwo = table.getPong();
            System.out.println(i + " " + textTwo);
        }
    }
}
